My modlist for The Elder Scrolls V: Skyrim Special Edition, sorted by category.

***Read the descriptions for compatibility - many of these mods need patches in order to play nice together!***

### See also

* [/r/skyrimmods](https://www.reddit.com/r/skyrimmods/)
* [/r/skyrimmods/wiki](https://www.reddit.com/r/skyrimmods/wiki/index)
* [SSE vs Classic](https://www.reddit.com/r/skyrimmods/wiki/sse_vs_classic) - even if you know the differences, there's a lot of useful information in regards to SSE in particular
* [Beginners Guide to Modding SSE/Classic](https://www.reddit.com/r/skyrimmods/wiki/begin2)
* [The Pheonix Flavor](https://thephoenixflavour.com/)
* [LOTD STEP Guide](https://wiki.nexusmods.com/index.php/User:Darkladylexy/Lexys_LOTD_SE)
* [Nordic Skyrim](https://www.nexusmods.com/skyrimspecialedition/mods/12562)
* [YASHed Guide](https://www.nexusmods.com/skyrimspecialedition/mods/23894)

### Tools I use

* [Mod Organizer 2](https://www.nexusmods.com/skyrimspecialedition/mods/6194)
* [LOOT](https://loot.github.io/)
* [SSEEdit](https://www.nexusmods.com/skyrimspecialedition/mods/164/)
* [zEdit](https://z-edit.github.io/)
* [BethINI](https://www.nexusmods.com/skyrimspecialedition/mods/4875)

# Modlist

### Fixes

* [Unofficial Skyrim Special Edition Patch](https://www.nexusmods.com/skyrimspecialedition/mods/266)
* [SSE Engine Fixes](https://www.nexusmods.com/skyrimspecialedition/mods/17230?)
* [PrivateProfileRedirector](https://www.nexusmods.com/skyrimspecialedition/mods/18860) INI file cacher
* [Better Dialogue Controls](https://www.nexusmods.com/skyrimspecialedition/mods/1429/?) + [Better MessageBox Controls](https://www.nexusmods.com/skyrimspecialedition/mods/1428/)

### User Interface

* [SkyUI](https://www.nexusmods.com/skyrimspecialedition/mods/12604)
* [RaceMenu](https://www.nexusmods.com/skyrimspecialedition/mods/19080)
* [Immersive HUD](https://www.nexusmods.com/skyrimspecialedition/mods/12440)
* [A Quality World Map](https://www.nexusmods.com/skyrimspecialedition/mods/5804/)
* [Your Own Thoughts](https://www.nexusmods.com/skyrimspecialedition/mods/762)
* [Smaller Vanilla Cursors](https://www.nexusmods.com/skyrimspecialedition/mods/20617)

### Gameplay and Quests

* [Experience](https://www.nexusmods.com/skyrimspecialedition/mods/17751)
* [Unbound](https://www.nexusmods.com/skyrimspecialedition/mods/6532)
* [Ordinator - Perks of Skyrim](https://www.nexusmods.com/skyrimspecialedition/mods/1137)
* [Cutting Room Floor](https://www.nexusmods.com/skyrimspecialedition/mods/276/?)
* [Convenient Horses](https://www.nexusmods.com/skyrimspecialedition/mods/9519)
* [Honed Metal](https://www.nexusmods.com/skyrimspecialedition/mods/12885)
* [The Paarthurnax Dilemma](https://www.nexusmods.com/skyrimspecialedition/mods/365)
* [All Thieves Guild Jobs Concurrently](https://www.nexusmods.com/skyrimspecialedition/mods/14883)

### Audio and Music

* [Audio Overhaul for Skyrim](https://www.nexusmods.com/skyrimspecialedition/mods/12466) + [Immersive Sounds Compendium](https://www.nexusmods.com/skyrimspecialedition/mods/523)
* [Sounds of Skyrim](https://www.nexusmods.com/skyrimspecialedition/mods/30658)
* [Hammering Sounds](https://www.nexusmods.com/skyrimspecialedition/mods/5592)

### Visuals & Environment

* [Natural and Atmospheric Tamriel](https://www.nexusmods.com/skyrimspecialedition/mods/12842)
* [Enhanced Lighting for ENB](https://www.nexusmods.com/skyrimspecialedition/mods/1377)
* [Realistic Water Two](https://www.nexusmods.com/skyrimspecialedition/mods/2182)
* [Myrkvoir](https://www.nexusmods.com/skyrimspecialedition/mods/28406)
* [Ethereal Clouds](https://www.nexusmods.com/skyrimspecialedition/mods/2393)
* [Subtle Wind FX](https://www.nexusmods.com/skyrimspecialedition/mods/24395)
* [Remove Ugly Torch Glow](https://www.nexusmods.com/skyrimspecialedition/mods/26687)
* [Reduced Glow FX](https://www.nexusmods.com/skyrimspecialedition/mods/20691?)

### Models & Textures

* [Windsong Immersive Character Overhaul](https://www.nexusmods.com/skyrimspecialedition/mods/2136)
  * [KS Hairdos Lite](https://www.nexusmods.com/skyrimspecialedition/mods/1932)
  * [Superior Lore-Friendly Hair](https://www.nexusmods.com/skyrim/mods/36510)
  * [Witcher 3 Eyes](https://www.nexusmods.com/skyrimspecialedition/mods/2921) + [Improved Eyes Skyrim](https://www.nexusmods.com/skyrimspecialedition/mods/9141)
  * [Northborn Scars](https://www.nexusmods.com/skyrimspecialedition/mods/720)
* Landscapes and Terrain
  * [Noble Skyrim](https://www.nexusmods.com/skyrimspecialedition/mods/21423)
  * [MystiriousDawn's HD Skyrim Overhaul](https://www.nexusmods.com/skyrimspecialedition/mods/19421)
  * [HQ Solitude](https://www.nexusmods.com/skyrimspecialedition/mods/23937?)
  * [Northern Shores](https://www.nexusmods.com/skyrimspecialedition/mods/27041)
  * [Better Falmer Ceiling Cave Texture](https://www.nexusmods.com/skyrimspecialedition/mods/17232)
  * [Stalhrim Source](https://www.nexusmods.com/skyrimspecialedition/mods/9447)
* Creatures
  * [Demonic Alduin](https://www.nexusmods.com/skyrimspecialedition/mods/5455)
  * [Rustic Daedra](https://www.nexusmods.com/skyrimspecialedition/mods/19272)
  * [Rustic Spriggan](https://www.nexusmods.com/skyrimspecialedition/mods/18107)
  * [Rustic Death Hound and Gargoyle](https://www.nexusmods.com/skyrimspecialedition/mods/17740)
* Items and Gear
  * [Rustic Clothing](https://www.nexusmods.com/skyrimspecialedition/mods/4703)
  * [Dragonbone Mastery](https://www.nexusmods.com/skyrimspecialedition/mods/2056) (Dragonbone weapons retexture)
* Objects
  * [Rustic Clutter Collection](https://www.nexusmods.com/skyrimspecialedition/mods/5795)
  * [Rustic Elder Scroll](https://www.nexusmods.com/skyrimspecialedition/mods/17757)
  * [Sharpening Wheel HD](https://www.nexusmods.com/skyrimspecialedition/mods/22801)
  * [Ancient Pottery](https://www.nexusmods.com/skyrimspecialedition/mods/24039)
  * [Realistic Dark Elf Urns](https://www.nexusmods.com/skyrimspecialedition/mods/21717)
  * [M17 Septim Retexture](https://www.nexusmods.com/skyrimspecialedition/mods/3154)
* Other
  * [Inferno](https://www.nexusmods.com/skyrimspecialedition/mods/29316)

### Items and Gear

* Armor & Cothing
  * [Immersive Armors](https://www.nexusmods.com/skyrimspecialedition/mods/3479)
  * [Authentic Legion](https://www.nexusmods.com/skyrimspecialedition/mods/29559)
  * [Masters of Death - Rise of the Brotherhood](https://www.nexusmods.com/skyrimspecialedition/mods/15448)
  * [Regal Huntsman Armor](https://www.nexusmods.com/skyrimspecialedition/mods/9368)
  * [Cloaks of Skyrim](https://www.nexusmods.com/skyrimspecialedition/mods/6369) + [Winter is Coming](https://www.nexusmods.com/skyrimspecialedition/mods/4933)
  * [Improved Closefaced Helmets](https://www.nexusmods.com/skyrimspecialedition/mods/824)
* Weapons
  * [Lore Weapon Expansion](https://www.nexusmods.com/skyrimspecialedition/mods/9660)
  * [Zim's Immersive Artifacts](https://www.nexusmods.com/skyrimspecialedition/mods/9138)
  * [LeanWolf's Better-Shaped Weapons](https://www.nexusmods.com/skyrimspecialedition/mods/2017)

### NPCs and Creatures

* [Immersive Patrols](https://www.nexusmods.com/skyrimspecialedition/mods/718)
* [Sickening Skeevers](https://www.nexusmods.com/skyrimspecialedition/mods/24428)
* [Hardy Hares](https://www.nexusmods.com/skyrimspecialedition/mods/27366)
* [Gritty Goats](https://www.nexusmods.com/skyrimspecialedition/mods/26665)
* [Dramatic Deer](https://www.nexusmods.com/skyrimspecialedition/mods/31010)
* [Bristleback Boars](https://www.nexusmods.com/skyrimspecialedition/mods/22578)
* [Savage Bear](https://www.nexusmods.com/skyrimspecialedition/mods/16343)
* [Immersive Smilodon](https://www.nexusmods.com/skyrimspecialedition/mods/18429)
* [Mighty Mammoths](https://www.nexusmods.com/skyrimspecialedition/mods/24237)
* [Grandiose Giants](https://www.nexusmods.com/skyrimspecialedition/mods/23889)
* [Notorious Netches](https://www.nexusmods.com/skyrimspecialedition/mods/29323)
* [Salty Slaughterfish](https://www.nexusmods.com/skyrimspecialedition/mods/28005)
* [Absolute Arachnophobia](https://www.nexusmods.com/skyrimspecialedition/mods/24058)
* [Dreaded Dwarven Spiders](https://www.nexusmods.com/skyrimspecialedition/mods/27047)
* [Riekling Reavers](https://www.nexusmods.com/skyrimspecialedition/mods/22948)
* [Riekling Roughriders](https://www.nexusmods.com/skyrimspecialedition/mods/22765)
* [Tyrannical Trolls](https://www.nexusmods.com/skyrimspecialedition/mods/23665)
* [Nightmare Chaurus](https://www.nexusmods.com/skyrimspecialedition/mods/21488)
* [Infamous Ice Wraiths](https://www.nexusmods.com/skyrimspecialedition/mods/29712)
* [Hellish Hounds](https://www.nexusmods.com/skyrimspecialedition/mods/30344)
* [Grave Gargoyles](https://www.nexusmods.com/skyrimspecialedition/mods/21907)
* [Looming Lurkers](https://www.nexusmods.com/skyrimspecialedition/mods/23122)
* [Supreme Vampire Lords](https://www.nexusmods.com/skyrimspecialedition/mods/19706)
* [Immersive Dragons](https://www.nexusmods.com/skyrimspecialedition/mods/18957)

### Followers and Companions

* [Nether's Follower Framework](https://www.nexusmods.com/skyrimspecialedition/mods/18076)
* [Go On Ahead](https://www.nexusmods.com/skyrimspecialedition/mods/4779)

### World

* [JK's Skyrim](https://www.nexusmods.com/skyrimspecialedition/mods/6289)
* [Kynesgrove](https://www.nexusmods.com/skyrimspecialedition/mods/351) + [Karthwasten](https://www.nexusmods.com/skyrimspecialedition/mods/350) + [Darkwater Crossing](https://www.nexusmods.com/skyrimspecialedition/mods/326) + [Whistling Mine](https://www.nexusmods.com/skyrimspecialedition/mods/367) + [Soljund's Sinkhole](https://www.nexusmods.com/skyrimspecialedition/mods/358) + [Shor's Stone](https://www.nexusmods.com/skyrimspecialedition/mods/354)
* [Keld-Nar](https://www.nexusmods.com/skyrimspecialedition/mods/14353) + [Telengard](https://www.nexusmods.com/skyrimspecialedition/mods/17423) + [Oakwood](https://www.nexusmods.com/skyrimspecialedition/mods/27564) + [Helarchen Creek](https://www.nexusmods.com/skyrimspecialedition/mods/4043)
* [Inns and Taverns](https://www.nexusmods.com/skyrimspecialedition/mods/12223)
* [Hidden Hideouts of Skyrim](https://www.nexusmods.com/skyrimspecialedition/mods/2625)
* [Lanterns of Skyrim II](https://www.nexusmods.com/skyrimspecialedition/mods/30817)

### Player Homes

* [Breezehome by Lupus](https://www.nexusmods.com/skyrimspecialedition/mods/9626/?)
* [Honeyside TNF](https://www.nexusmods.com/skyrimspecialedition/mods/4935)
* [Beginner's Shack in Riverwood](https://www.nexusmods.com/skyrimspecialedition/mods/6238)
* [Routa](https://www.nexusmods.com/skyrimspecialedition/mods/1193)
* [Ruska](https://www.nexusmods.com/skyrimspecialedition/mods/16177)
* [Halla](https://www.nexusmods.com/skyrimspecialedition/mods/1940/?)
* [Maple Forest House](https://www.nexusmods.com/skyrimspecialedition/mods/31033)
* [Lake Haven](https://www.nexusmods.com/skyrimspecialedition/mods/2860)
* [Mammoth Manor](https://www.nexusmods.com/skyrimspecialedition/mods/9710)
* [Cliffside Cottage](https://www.nexusmods.com/skyrimspecialedition/mods/4036)
* [Windyridge](https://www.nexusmods.com/skyrimspecialedition/mods/8046)
* [Harbourside](https://www.nexusmods.com/skyrimspecialedition/mods/17959)
* [Winter Cove](https://www.nexusmods.com/skyrimspecialedition/mods/4171)
* [Viking's Longhouse](https://www.nexusmods.com/skyrimspecialedition/mods/10860)
* [Riften Fishing Shack](https://www.nexusmods.com/skyrimspecialedition/mods/4046)
* [Pathfinder's Shack](https://www.nexusmods.com/skyrimspecialedition/mods/24911)
* [Snapneck Shack](https://www.nexusmods.com/skyrimspecialedition/mods/4199)
* [Lost Snowy Shack](https://www.nexusmods.com/skyrimspecialedition/mods/24181)

### ENB Presets

* [Photorealistic Tamriel](https://www.nexusmods.com/skyrimspecialedition/mods/4743)
* [Mythical](https://www.nexusmods.com/skyrimspecialedition/mods/11660)
